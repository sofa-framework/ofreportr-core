% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/mesh.R
\name{extract_repCellSize}
\alias{extract_repCellSize}
\title{Calculates a representative cell size}
\usage{
extract_repCellSize(casePath)
}
\arguments{
\item{casePath}{character string with path to the case}
}
\value{
a data.table with the representative cell size
}
\description{
calculates a representative cell size according to >Celic, 2008 - Procedure
forestimation and reporting of uncertainty due to descritization in CFD
applications< as the cuberoot of the total volume of the domain divided by
the number of cells in the domain.
}
\examples{
coverage <- extract_multiLayerCoverage(".")
}
