# OFReportR

<!-- badges: start -->
<!-- badges: end -->

The goal of OFReportR is to enable you to create html reports from the various
OpenFOAM Log files and outputs from postProcessing. OFReportR will do all the 
parsing in the background for you and provide that data in the common R form of 
data.tables, so that it is up to you to display them as you like then. 

## Installation

You can install the released version of OFReportR from gitLab

``` r
# install.packages("withr")
# install.packages("devtools")
devtools::install_git("https://gitlab.com/sofa-framework/ofreportr-core")
```
