#' Load or install R packages
#'
#'Loads a package if it has already been installed. If not the function will
#'try to download and install the package
#' @param package_name string with name of the package
#'
#' @return loads or installs the package in question
#' @export
#' @import data.table
#' @import stringr
#' @import rjson
#' @examples
#' load_or_install("data.table")
################################################################################

load_or_install <- function(package_name) {
  if(!is.element(package_name, installed.packages()[, 1]))
  {
    install.packages(package_name,repos="http://lib.stat.cmu.edu/R/CRAN")
  }
  library(package_name,character.only=TRUE,quietly=TRUE,verbose=FALSE)
}


#' Parse cpp style skripts
#'
#' Read any OpenFOAM Cpp syle script and save the data into a nested list
#' sorted by the identifiers. Strips semicolons from the script
#'
#' @param filePath path to file
#'
#' @return List nested and named according to the supplied script
#' @export
#' @examples
#' fvSolution <- read_foamScript("./system/fvSolution")
################################################################################

read_foamScript <- function(filePath) {

  # declare internal function
  internalBlock <- function(lastLine) {
    # declare variables
    currentBlock  <- list()
    blockUp       <- list()
    # browse file until last line
    while (currentLine <= lastLine) {
      # skip empty lines
      if ( fileData[currentLine] == "" ) {
        currentLine <<- currentLine + 1
        # skip commented lines
      } else if ( stringr::str_sub(stringr::str_trim(fileData[currentLine]),1,2) == "//" ) {
        currentLine <<- currentLine + 1
        # skip multiline comments
      } else if (stringr::str_sub(stringr::str_trim(fileData[currentLine]),1,2) == "/*"){
        while (stringr::str_sub(stringr::str_trim(fileData[currentLine]),-2,-1) != "*/") {
          currentLine <<- currentLine + 1
        }
        currentLine <<- currentLine + 1
        # remove inline comments
      } else if (length(unlist(stringr::str_split(stringr::str_trim(fileData[currentLine]), pattern = "//"))) > 1) {
        fileData[currentLine] <- stringr::str_trim(stringr::str_split(fileData[currentLine], pattern = "//")[[1]][1])
        # find opening brace
      } else if (stringr::str_trim(fileData[currentLine]) == "{") {
        currentLine <<- currentLine + 1
        blockUp <- internalBlock(lastLine)            # call subroutine to handle everything inside braces
        blockUp <- list(blockUp)                      # make returned values a single list
        names(blockUp) <- identifier                  # name the list
        currentBlock <- append(currentBlock,blockUp)  # merge the lists
        blockUp <- list()                             # clear blockUp for next call
        # find closing brace
      } else if (stringr::str_trim(fileData[currentLine]) == "}") {
        currentLine <<- currentLine + 1
        return(currentBlock)                          # return currentBlock to calling routine
        # find entrys
      } else {
        # if entry ends with ";" it's a regular entry
        if ( stringr::str_sub(fileData[currentLine], -1, -1) == ";") {
          # if entry contains more than 1 word it's a regular entry
          if (length(stringr::str_split(stringr::str_trim(fileData[currentLine]), "\\s+")[[1]]) > 1) {
            entry <- list(stringr::str_split(stringr::str_trim(fileData[currentLine]),"\\s+"))
            names(entry) <- entry[[1]][[1]][1]
            entry[[1]][[1]] <- stringr::str_sub(paste(entry[[1]][[1]][2:length(entry[[1]][[1]])], collapse = " "),1,-2)
            currentBlock <- append(currentBlock, entry)
            entry <- list()
            # if entry contains only 1 word it's a reference
          } else {
            entry <- list(stringr::str_sub(stringr::str_split(stringr::str_trim(fileData[currentLine]), "\\s+"), 1, -2))
            names(entry) <- entry[[1]][[1]]
            currentBlock <- append(currentBlock, entry)
            entry <- list()
          }
          # every word without ";" is an identifier for a block
        } else {
          identifier <- paste0(unlist(stringr::str_split(stringr::str_trim(fileData[currentLine]),"\\s+")),collapse = "")
        }
        currentLine <<- currentLine + 1
      }
    }
    return(currentBlock)
  }

  # declare variables with global scope so all subroutines have access
  fileData    <<- readLines(filePath)
  currentLine <<- 1
  lastLine    <<- length(fileData)

  # start
  mainBlock <- internalBlock(lastLine)
  # clean up global variables
  # rm(currentLine,fileData,lastLine,inherits = TRUE)
  return(mainBlock)
}


#' Remove symlinks from path vector
#'
#' Removes paths which contain a symlink from a vector of paths
#'
#' @param paths vector containing paths
#' @param excecute weater to remove symlinks or not
#' @param skip how many folders to skip from the beginning of \code{paths}
#' @return Vector of paths without symlinks
#' @export
#' @examples
#' paths <- list.dirs(".")
#' noSymlinks <- removeSymlinks(paths)
################################################################################

removeSymlinks <- function(paths, excecute = TRUE, skip=0) {

  if (excecute) {
    # function to check for symlink in a path
    is.symlink <- function(paths) isTRUE(nzchar(Sys.readlink(paths), keepNA=TRUE))
    # initialize variable
    noSymlinks <- c()
    # loop over paths
    for (path in paths) {
      temp <- path
      # loop over all directories of path to check each for symlink except first
      # two (../../)
      for (i in 1:(length(unlist(stringr::str_split(path, "/")))-skip)) {
        # break if any folder is symlink
        if (is.symlink(temp)) {
          path <- ""
          break
        }
        # decrease path by one folder
        temp <- dirname(temp)
      }
      # assign path to vector if there was no symlink
      if (path != "") {
        noSymlinks <- c(noSymlinks, path)
      }
    }
    return(noSymlinks)
  } else {
    return(paths)
  }
}


#' Finds OpenFOAM case directories
#'
#' Finds OpenFOAM case directories recursive in supplied path
#'
#' @param studyPath string containing a path
#' @param printable weather to make the paths printable (default = FALSE)
#' @param excludeMesh weather to exclude mesh cases (default = FALSE)
#' @param skipSymlinks weather to accept symlinks (default = FALSE)
#' @return Vector of OpenFOAM case directories
#' @export
#' @examples
#' OFCases <- find_caseDirs(".")
################################################################################

find_caseDirs <- function(studyPath, printable = FALSE, excludeMesh = FALSE, skipSymlinks = FALSE){

  # Find all "system" folders as unique identifier
  caseDirs <- dir(studyPath,recursive = TRUE, pattern = "system", include.dirs = TRUE , full.names = !printable)
  # ERROR
  if (length(caseDirs) == 0) {
    warning(paste0("No OpenFoam cases found in ", path))
    return(c(""))
  }
  if (excludeMesh) {
    caseDirs <- caseDirs[! stringr::str_detect(stringr::str_to_lower(caseDirs), "mesh[0-9][0-9][0-9]")]
  }
  if (skipSymlinks) {
    caseDirs <- removeSymlinks(caseDirs)
  }
  if (printable) {
    caseDirsTable       <- data.table::data.table(case = character(0), path = character(0))
    caseDirsTable_temp  <- data.table::data.table(case = character(1), path = character(1))
    for (caseNo in 1:length(caseDirs)){
      # cut "/system" from paths to find bare case directory
      caseDirsTable_temp$case <- paste0("case", stringr::str_pad(caseNo, 3, side = "left", pad="0"))
      caseDirsTable_temp$path <- stringr::str_sub(caseDirs[caseNo],1,-8)
      caseDirsTable <- rbind(caseDirsTable,caseDirsTable_temp)
    }
    return(caseDirsTable)
  } else {
    for (caseNo in 1:length(caseDirs)){
      # cut "/system" from paths to find bare case directory
      caseDirs[caseNo] <- stringr::str_sub(caseDirs[caseNo],1,-8)
    }
    return(caseDirs)
  }
}

#' Extract name of case directory
#'
#' Extracts the name of the folder containing the OpenFOAM case
#'
#' @param casePath string containing a path
#' @return name of directory
#' @export
#' @examples
#' OFCases <- find_caseDirs(".")
#' OFCaseName <- get_caseName(OFCases[1])
################################################################################

get_caseName <- function(casePath) {

  # convert symlinks to full path
  fullPath <- normalizePath(casePath)
  # split into single dirs
  splitPath <- unlist(stringr::str_split(fullPath, "/"))
  # get case name
  caseName <- splitPath[length(splitPath)]
  return(caseName)
}

#' Extract name of project
#'
#' Extracts the name of a folder containing multiple OpenFOAM cases
#'
#' @param casePath string containing a path
#' @return name of directory
#' @export
#' @examples
#' OFCases <- find_caseDirs(".")
#' OFProjectName <- get_projectName(OFCases[1])
################################################################################

get_projectName <- function(casePath) {

  # convert symlinks to full path
  fullPath <- normalizePath(casePath)
  # split into single names
  splitPath <- unlist(stringr::str_split(fullPath, "/"))
  # get project name
  projectName <- splitPath[length(splitPath)-2]
  return(projectName)
}


#' Find path to project.json
#'
#' Finds the path to project.json from anywhere inside a project
#'
#' @param startingPath string with path anywhere inside a project
#' @return path to project.json
#' @export
#' @examples
#' projectJsonPath <- find_projectJsonPath(".")
################################################################################

find_projectJsonPath <- function(startingPath) {

  # initialize variables
  projectJsonPath <- character(0)
  if (length(startingPath > 1)) {
    currentPath <- startingPath[1]
  } else {
    currentPath <- startingPath
  }
  i=0
  # walk file tree uo until project.json is found
  while ( (length(projectJsonPath) < 1) & (i < 5)  ) {
    projectJsonPath <- list.files(path = currentPath,
                                  pattern = "*project*(\\.json)$",
                                  full.names = TRUE)
    #projectJsonPath <- removeSymlinks(projectJsonPath)
    currentPath <- paste0(currentPath, "/..")
    i <- i + 1
  }
  # get non relative path
  projectJsonPath <- normalizePath(projectJsonPath)
  return(projectJsonPath)
}


#' Extracts aspect paths
#'
#' Extracts paths to a certain aspects from a list of case paths
#'
#' @param casePaths vector of case paths
#' @param aspect string containing aspect: cad, run, mesh or survey
#' @return vector of case paths limited to chosen aspect
#' @export
#' @examples
#' meshPaths <- extract_aspectPaths(".", "mesh")
################################################################################

extract_aspectPaths <- function(casePaths, aspect){

  # find project json
  projectJsonPath <- find_projectJsonPath(casePaths[1])
  # load json
  json <- fromJSON(file = projectJsonPath)
  # extract selected aspect name
  if (aspect == "cad") {
    AspectName     <- json$foamStructure$cad$aspectName
  } else if (aspect == "mesh"){
    AspectName    <- json$foamStructure$mesh$aspectName
  } else if (aspect == "run"){
    AspectName     <- json$foamStructure$run$aspectName
  } else if (aspect == "survey"){
    AspectName  <- json$foamStructure$survey$aspectName
  } else {
    warning(paste0("Unknown aspect name >", aspect,
                   "<. Valid aspects are: cad, mesh, run, survey"))
    return(c(""))
  }
  # convert symlinks to full path
  fullPaths <- normalizePath(casePaths)
  # get subset of paths
  aspectPaths <- stringr::str_subset(string = fullPaths, pattern = AspectName)
  return(aspectPaths)
}


#' Combine data.tables from different extraction models
#'
#' Combines data.tables from different extraction models for multiple cases
#'
#' @param extract_model a function for an extraction (eg. extract_BC)
#' @param studyPath path to a study
#' @param arg2 additional argument if needed by extraction model
#' @param excludeMesh weather to exclude mesh folders
#' @return a data.table with the content of all cases and a case identifier
#' @export
#' @examples
#' BCs <- combine_frames(extract_BC, ".")
################################################################################

combine_frames <- function(extract_model,studyPath,arg2,excludeMesh=FALSE){

  # find cases
  caseDirs <- find_caseDirs(studyPath, printable =  FALSE, excludeMesh = excludeMesh)
  # initialize variables
  frame <- data.table::data.table()
  for (currentCase in caseDirs) {
    # extract data from cases to temporary data.frames
    if (missing(arg2)){
      frame_temp <- extract_model(currentCase)
    } else {
      frame_temp <- extract_model(currentCase,arg2)
    }
    # add column to identify meshes
    frame_temp[, case := basename(currentCase) ]
    # combine all cases into one data.table
    frame <- rbind(frame,frame_temp)
  }
  return(frame)
}

#' Transpose a data.table about selected coloumn
#'
#' Switches rows and columns of a data.table
#'
#' @param frame a data.table
#' @param byColumnName Name of the column to turn about
#' @return a data.table inverted around the selected column
#' @export
#' @examples
#' MeshStats <- invert_frame(MeshStats, "Description")
################################################################################

invert_frame <- function(frame, byColumnName) {

  if (byColumnName %in% colnames(frame) ) {
    # initialise vector with unique rownames
    #cols <- unique(frame[,byColumnName, with=FALSE])
    cols <- as.matrix(frame[,byColumnName,with = FALSE])[,1]
    # initialise data.table with unique rows
    temp_frame <- data.table::as.data.table(matrix(nrow = 1, ncol = length(cols)))
    # convert all fields to character
    temp_frame[,] <- character(1)
    # name rows
    colnames(temp_frame) <- cols
    for (j in 1:length(cols) ) {
      temp_frame[1, j] <- as.character(unlist(frame[j, 2]))
    }
  } else{
    warning("No column ",  byColumnName, " in ", deparse(substitute(frame)))
    return(frame)
  }
  return(temp_frame)
}


#' Concentrate data.tables to unique entrys
#'
#' Reduces a the data.table to unique entrys and displays for which case the
#' entrys are valid
#'
#' @param combinedTable a data.table containing data of multiple cases
#' @param byColumnName Name of the column to turn about
#' @return a data.table with the content reduced to unique entrys
#' @export
#' @examples
#' BCTable <- concentrate_Tables(combine_frames(extract_BC, path))
################################################################################

concentrate_Tables <- function(combinedTable) {

  # define looping function
  checkColumn <- function(passedTable, tableCol){
    # loop over all unique entrys in one column of a data.table
    for (selector in unname(unlist(unique(passedTable[, ..tableCol])))) {
      # create a Subselection with all entrys matching one unique entry of the column
      tableSelection <- passedTable[as.vector(passedTable[, ..tableCol] == selector)]
      # if second to last column is reached save into a data.frame (last column is case no.)
      if ( tableCol == (ncol(tableSelection) - 1)) {
        lastCol <- ncol(tableSelection)
        # save all applying cases into single string
        cases   <- unlist(unique(tableSelection[, ..lastCol]))
        # delete last column (case no.)
        tableSelection[, (lastCol):=NULL]
        # save (arbitrarily) first column as representative for all cases
        concentrated_temp       <- tableSelection[1, ]
        # save cases into new last column
        concentrated_temp$cases <- paste(cases, collapse = "\n")
        # save into global table
        concentrated            <- rbind(concentrated, concentrated_temp)
      } else {
        # access next column
        returnedTable   <- checkColumn(tableSelection, tableCol + 1 )
        # save into global table
        concentrated  <- rbind(concentrated, returnedTable)
      }
    }
    return(concentrated)
  }
  # initialize global table
  concentrated <- data.table::data.table()
  # make sure passed table has no NA's
  combinedTable[is.na(combinedTable)] <- ""
  # start concentration process
  concentratedTable <- checkColumn(combinedTable, 1)
  return(concentratedTable)
}


#' Extract OpenFOAM version
#'
#' Extracts the OpenFoam version from a log file
#'
#' @param casePath path to a case
#' @return string with the OpenFOAM version
#' @export
#' @examples
#' OFversion <- extract_OFVersion(casePath = ".")
################################################################################

extract_OFVersion <- function(casePath) {

  # Define function to extract the version from a file
  find_OFVersion <- function(logFile) {
    # ensure logfile is not empty
    if ( ! file.info(logFile)$size == 0) {
      logFile <- readLines(logFile, n = 30)
      for (i in seq(8, length(logFile))){
        # find unique entry "Build" and save the version
        if ((stringr::str_split(stringr::str_trim(logFile[i]), "\\s+")[[1]][1] == "Build")) {
          OFversion <- stringr::str_split(stringr::str_trim(logFile[i]), "\\s+")[[1]][3]
          return(OFversion)
          break
        }
      }
    }
    return(NA)
  }

  # List all files starting with log. ending with .log
  logFiles <- list.files(casePath,recursive=TRUE, pattern="(?i)(?i)(^(log\\.)(.*)|(.*)(\\.log)$)", full.names = TRUE)
  # remove files containing a symlink
  logFiles <- removeSymlinks(logFiles)
  # throw warning if no logfile is present
  if (length(logFiles) < 1){
    warning(paste0("No log files in ", casePath))
    return(NA)
  }
  # get file infos
  logFiles <- file.info(logFiles)
  # sort by modification date
  logFiles <- logFiles[with(logFiles, order(as.POSIXct(mtime), decreasing = TRUE)), ]
  # turn back to vector
  logFiles <- rownames(logFiles)
  # select newest item
  i <- 1
  # try to extract version from file
  OFversion <- find_OFVersion(logFiles[i])
  # if  version could not be extracted keep going until all files are tested
  while (is.na(OFversion) & i < length(logFiles) ) {
    i <- i+1
    OFversion <- find_OFVersion(logFiles[i])
  }
  if (is.na(OFversion)) {
    warning(paste("Could not extract a version from any logfile in:", normalizePath(casePath) ))
    return(NA)
  }
  return(OFversion)
}


#' Filter last timestep for each case from a data.table
#'
#' Filter last timestep for each case from a data.table
#'
#' @param frame the data.table to scan
#' @return filtered data.table
#' @export
#' @examples
#' filtered <- filter_lastTimesteps()
################################################################################

filter_lastTimesteps <- function(frame){

  result <- data.table::data.table()
  # check if frame has columns named study and time
  if ( ("case" %in% colnames(frame)) & ("time" %in% colnames(frame)) ) {
    for (i in 1:length(unique(frame$study))) {
      temp_frame <- frame[(frame$study==unique(frame$study)[i])]
      temp_frame <- temp_frame[temp_frame$time== max(temp_frame$time)]
      result <- rbind(result, temp_frame)
    }
  } else {
    warning("One or all of the columns 'study' or 'time' are missing in ", deparse(substitute(frame)))
    return(frame)
  }
  return(result)
}


#' Combine details into a single data.table
#'
#' @param casePaths paths to cases
#'
#' @return cumulated and inverted data.table
#' @export
#'
#' @examples
#' casePaths <- c("mesh001", "mesh002", "mesh003")
#' meshDetails combine_meshDetails(casePaths)
################################################################################

combine_meshDetails <- function(casePaths){

  # initialize data.table
  details <- data.table::data.table()
  # loop over all cases
  for (case in casePaths){
    # extract data for the case
    temp <- extract_meshDetails(case)
    # transpose the dtaa.table
    temp <- invert_frame(temp, "V1")
    #combine all data into single data.table
    details <- rbind(details, temp )
  }

  return(details)
}
